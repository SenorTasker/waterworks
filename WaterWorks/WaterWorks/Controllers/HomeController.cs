﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WaterWorks.Models;

namespace WaterWorks.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
