﻿using Microsoft.AspNetCore.Mvc;

namespace WaterWorks.Controllers
{
    public class AboutController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult ManagementCommittee()
        {
            return View();
        }
    }
}