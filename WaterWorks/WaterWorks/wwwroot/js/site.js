﻿function project(selectableElements, source, target) {
    selectableElements.forEach(item => {
        if (item == source) {
            target.src = source.getAttribute("src").replace('Small', 'Large');
            item.classList.add('selected')
        }
        else {
            item.classList.remove('selected')
        }
    });
}